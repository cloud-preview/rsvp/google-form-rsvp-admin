const fs = require('node:fs').promises;
const path = require('node:path');

const {auth} = require('google-auth-library');
const {google} = require('googleapis');

const {ControllerMixinView, KohanaJS} = require('kohanajs');
const {ControllerAdmin} = require('@kohanajs/mod-admin');

class ControllerAdminHome extends ControllerAdmin {
    constructor(request) {
        super(request, null, {
            roles: new Set(['staff', 'moderator']),
            templates: new Map([
                ['index', 'templates/admin/form-responses/index'],
                ['read', 'templates/admin/form-responses/read'],
                ['edit', 'templates/admin/form-responses/read'],
            ]),
        });
    }

    async action_index() {
        const strKey = await fs.readFile(path.normalize(KohanaJS.config.google_form.rsvp.credentials), 'utf-8');
        const client = auth.fromJSON(JSON.parse(strKey));
        client.scopes = [
            'https://www.googleapis.com/auth/spreadsheets'
        ];

        const sheets = google.sheets({version: 'v4', auth:client});
        const res = await sheets.spreadsheets.values.get({
            spreadsheetId: KohanaJS.config.google_form.rsvp.spreadsheetId,
            range: 'Form Responses 1!A2:F',
        });
        const rows = res.data.values;

        const items = rows.map((row, i) => ({
            id: i+2,
            created_at: row[0],
            email: row[1],
            rsvp: row[2],
            name: row[3],
            source: row[4],
            comment: row[5]
        }));

        Object.assign(
            this.state.get(ControllerMixinView.TEMPLATE).data,
            {items}
        )

    }
}

module.exports = ControllerAdminHome;
