const { KohanaJS } = require('kohanajs');
KohanaJS.initConfig(new Map([
  ['google_form', require('./config/google_form')],
]));