const {KohanaJS} = require('kohanajs');

module.exports = {
    rsvp:{
        spreadsheetId: '18DOJcG1bUJsEmU2qjOTYIJzd4uU9ZdSXgBpNAuGgftA',
        credentials: KohanaJS.EXE_PATH + '/../keys/credentials.json',
        formId: '1PzARXWHuTwHIqeY5559R39VDCDQMXPtcV9NdxR1ZTfk'
    }
};
